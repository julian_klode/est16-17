package auffuehrungssystem;

public class Suchmaske {
	private String text;
	private boolean sucheUntertitel;
	private Sortierkriterium sortierkriterium;

	public Suchmaske(String text, boolean sucheUntertitel, Sortierkriterium sortierkriterium) {
		super();
		this.text = text;
		this.sucheUntertitel = sucheUntertitel;
		this.sortierkriterium = sortierkriterium;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public boolean isSucheUntertitel() {
		return sucheUntertitel;
	}

	public void setSucheUntertitel(boolean sucheUntertitel) {
		this.sucheUntertitel = sucheUntertitel;
	}

	public Sortierkriterium getSortierkriterium() {
		return sortierkriterium;
	}

	public void setSortierkriterium(Sortierkriterium sortierkriterium) {
		this.sortierkriterium = sortierkriterium;
	}
}
